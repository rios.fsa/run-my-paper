{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Deep Learning Seismic Events Classification\n",
    "## Requirements \n",
    "\n",
    "The following list shows the required Python libraries and frameworks to run this notebook:\n",
    "\n",
    "* H5Py (version 2.10.0)\n",
    "* Keras (version 2.4.3)\n",
    "* Numpy (version 1.19.2)\n",
    "* Scikit-Learn (version 0.24.1)\n",
    "* TensorFlow (version 2.4.1)\n",
    "\n",
    "You can install them by just runing the cell bellow:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install h5py~=2.10.0 keras==2.4.3 numpy~=1.19.2 scikit-learn==0.24.1 tensorflow==2.4.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading Dependencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import h5py\n",
    "import functools\n",
    "import numpy as np\n",
    "import tensorflow as tf\n",
    "\n",
    "from sklearn import metrics\n",
    "from sklearn import preprocessing\n",
    "from keras.models import Sequential\n",
    "from keras.callbacks import EarlyStopping\n",
    "from keras.wrappers.scikit_learn import KerasClassifier\n",
    "from sklearn.model_selection import StratifiedKFold as KFold"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## SeismicNet\n",
    "\n",
    "Next, we create our network architecture: SeismicNet. Thus, we implemented a class to define our training strategy as well as the network architecture.  For the sake of an easy demonstration, we defined in this notebook the network training and evaluation as simply as possible:  a 3-fold stratified cross-validation strategy with 5 epochs. Also, we are only collecting three metrics to measure the network performance: accuracy, f1-score, and confusion matrix."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "class SeismicNet:\n",
    "    def __init__(self, X, y, epochs=5, folds=3,\n",
    "                 patience=1, batch_size=32) -> None:\n",
    "        self.epochs = epochs\n",
    "        self.folds = folds\n",
    "        self.patience = patience\n",
    "        self.batch_size = batch_size\n",
    "\n",
    "        self.X = X\n",
    "        self.y = y\n",
    "\n",
    "    def _create(self): \n",
    "        model = Sequential([\n",
    "            # Input Layer\n",
    "            tf.keras.Input(shape=self.X[0].shape),\n",
    "\n",
    "            # Feature Layers\n",
    "            tf.keras.layers.Conv1D(32, 64, strides=2, padding=\"same\",\n",
    "                                   activation='relu'),\n",
    "            tf.keras.layers.MaxPooling1D(pool_size=8),\n",
    "            tf.keras.layers.Dropout(0.15),\n",
    "\n",
    "            tf.keras.layers.Conv1D(64, 32, strides=2, padding=\"same\",\n",
    "                                   activation='relu'),\n",
    "            tf.keras.layers.MaxPooling1D(pool_size=8),\n",
    "            tf.keras.layers.Dropout(0.35),\n",
    "\n",
    "            tf.keras.layers.Conv1D(128, 16, strides=2, padding=\"same\",\n",
    "                                   activation='relu'),\n",
    "            tf.keras.layers.MaxPooling1D(pool_size=2),\n",
    "\n",
    "            tf.keras.layers.Conv1D(256, 8, strides=2, padding=\"same\",\n",
    "                                   activation='relu'),\n",
    "            tf.keras.layers.Conv1D(1401, 16, strides=12, padding=\"same\",\n",
    "                                   activation='relu'),\n",
    "\n",
    "            # Transformation Layer\n",
    "            # Flattens the input Layers\n",
    "            tf.keras.layers.Flatten(),\n",
    "\n",
    "            # Classification Layers\n",
    "            # Core Layer - Dense(number of nodes, activation function)\n",
    "            # Noise Layer (Used to avoid overfiting) - Dropout\n",
    "            tf.keras.layers.Dense(1500, activation='relu'),\n",
    "            tf.keras.layers.Dropout(0.75),\n",
    "\n",
    "            # Output Layers\n",
    "            tf.keras.layers.Dense(len(self.y[0])),\n",
    "            tf.keras.layers.Activation(\"softmax\")\n",
    "        ])\n",
    "\n",
    "        # Model configuration\n",
    "        model.compile(optimizer=tf.keras.optimizers.Adamax(),\n",
    "                      loss='categorical_crossentropy',\n",
    "                      metrics=['accuracy'])\n",
    "\n",
    "        return model\n",
    "\n",
    "    def train(self):\n",
    "        estimator = KerasClassifier(build_fn=self._create, epochs=self.epochs,\n",
    "                                    batch_size=self.batch_size, verbose=1)\n",
    "\n",
    "        # fix random seed for reproducibility\n",
    "        seed = 42\n",
    "        np.random.seed(seed)\n",
    "\n",
    "        # Stritified K-Fold\n",
    "        kfold = KFold(n_splits=self.folds, shuffle=True, random_state=seed)\n",
    "\n",
    "        accuracies = []\n",
    "        f1_scores = []\n",
    "        confusion_matrices = []\n",
    "\n",
    "        earlystop = EarlyStopping(monitor='loss', min_delta=0.001,\n",
    "                                  patience=self.patience, verbose=1)\n",
    "\n",
    "        for fold, (train, test) in \\\n",
    "                enumerate(kfold.split(self.X, np.argmax(self.y, axis=1)), 1):\n",
    "            print(f\"\\n-= Fold: {fold} =-\\n\")\n",
    "\n",
    "            estimator.fit(self.X[train], self.y[train], callbacks=[earlystop])\n",
    "\n",
    "            y_inv = np.argmax(self.y[test], axis=1)\n",
    "            y_pred = estimator.predict(self.X[test])\n",
    "\n",
    "            cm = metrics.confusion_matrix(y_inv, y_pred)\n",
    "            f_measure = np.mean(metrics.f1_score(y_inv, y_pred, average=None))\n",
    "            acc = metrics.accuracy_score(y_inv, y_pred)\n",
    "\n",
    "            confusion_matrices.append(cm)\n",
    "            f1_scores.append(f_measure)\n",
    "            accuracies.append(acc)\n",
    "\n",
    "        return {\n",
    "            \"accuracy\": (f\"{np.mean(accuracies) * 100:.2f}% \"\n",
    "                         f\"(+/- {np.std(accuracies) * 100:.2f}%)\"),\n",
    "\n",
    "            \"f1_score\": (f\"{np.mean(f1_scores) * 100:.2f}% \"\n",
    "                         f\"(+/- {np.std(f1_scores) * 100:.2f}%)\"),\n",
    "\n",
    "            \"confusion_matrix\": np.matrix(functools.reduce(np.add,\n",
    "                                                           confusion_matrices))\n",
    "        }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dataset\n",
    "\n",
    "The full data is available at http://dx.doi.org/10.17632/dv8nwdd36k.1, please download the files and set the path in the function `load_data()`.\n",
    "\n",
    "> _The dataset used in our analysis was published as \"Llaima volcano dataset: In-depth comparison of deep artificial neural network architectures on seismic events classification\" (available at https://doi.org/10.1016/j.dib.2020.105627) and our analyses were published as \"In-depth comparison of deep artificial neural network architectures on seismic events classification\" (available at https://doi.org/10.1016/j.jvolgeores.2020.106881)._"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def load_data(folder, classes):\n",
    "    if len(classes) < 2:\n",
    "        raise ValueError(\"You must choose at least two classes\")\n",
    "\n",
    "    lb = preprocessing.LabelBinarizer()\n",
    "    lb.fit(list(range(len(classes))))\n",
    "\n",
    "    X = []\n",
    "    y = []\n",
    "\n",
    "    for i, c in enumerate(classes):\n",
    "        with h5py.File(f\"{folder}/{c}.hdf5\", \"r\") as hdf5_file:\n",
    "            print(f\"Reading class data: {c.upper()}\")\n",
    "\n",
    "            data = np.asarray(hdf5_file[c.upper()]).reshape(-1, 6000, 1)\n",
    "            X.append(data)\n",
    "            y.append(np.full((data.shape[0], 1), i))\n",
    "\n",
    "    return np.vstack(X), lb.transform(np.vstack(y))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Experiment\n",
    "\n",
    "Finally, our small experiment can be computed on cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "X, y = load_data(folder='./content', classes=[\"lp\", \"tr\", \"vt\", \"tc\"])\n",
    "net = SeismicNet(X, y)\n",
    "\n",
    "results = net.train()\n",
    "\n",
    "print(\"\\n\\n=================================================\\n\")\n",
    "print(f\"Accuracy: {results['accuracy']}\\n\")\n",
    "print(f\"F1-score: {results['f1_score']}\\n\")\n",
    "print(f\"Confusion Matrix:\\n\\n{results['confusion_matrix']}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Feel free to change any hyperparameter to check differences in the network classification performance."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Citation\n",
    "\n",
    "@article{CANARIO2020106881,<br>\n",
    "title = {In-depth comparison of deep artificial neural network architectures on seismic events classification},<br>\n",
    "journal = {Journal of Volcanology and Geothermal Research},<br>\n",
    "volume = {401},<br>\n",
    "pages = {106881},<br>\n",
    "year = {2020},<br>\n",
    "issn = {0377-0273},<br>\n",
    "doi = {https://doi.org/10.1016/j.jvolgeores.2020.106881},<br>\n",
    "url = {https://www.sciencedirect.com/science/article/pii/S0377027319306171},<br>\n",
    "author = {João Paulo Canário and Rodrigo Mello and Millaray Curilem and Fernando Huenupan and Ricardo Rios}<br>\n",
    "}<br>\n",
    "\n",
    "@article{CANARIO2020105627,<br>\n",
    "title = {Llaima volcano dataset: In-depth comparison of deep artificial neural network architectures on seismic events classification},<br>\n",
    "journal = {Data in Brief},<br>\n",
    "volume = {30},<br>\n",
    "pages = {105627},<br>\n",
    "year = {2020},<br>\n",
    "issn = {2352-3409},<br>\n",
    "doi = {https://doi.org/10.1016/j.dib.2020.105627},<br>\n",
    "url = {https://www.sciencedirect.com/science/article/pii/S2352340920305217},<br>\n",
    "author = {João Paulo Canário and Rodrigo Fernandes {de Mello} and Millaray Curilem and Fernando Huenupan and Ricardo Araujo Rios}<br>\n",
    "} \n",
    "\n",
    "if you have any questions, send us a message:<br>\n",
    "* jopacanario@gmail.com\n",
    "* ricardoar@ufba.br"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}