This project aims at helping you to "run" all papers published by researchers of CINO (Computational Intelligence and Optimization Research Lab) from the Federal University of Bahia (Brazil).

Here you'll find links to our papers as well as their pocket versions, probably, summarized in Jupyter Notebooks.

If you have any questions, please send us a message.

#runmypaper and enjoy it!
